package chooser

import (
	"encoding/json"
	"os"
)

type Config struct {
	MongoURI   string `json:"mongoURI"`
	Database   string `json:"database"`
	Collection string `json:"collection"`
}

const configFileName = "config.json"

func LoadConfig() (config *Config, err error) {
	var file *os.File
	if file, err = os.Open(configFileName); err == nil {
		config = new(Config)
		err = json.NewDecoder(file).Decode(&config)
	}
	return
}
