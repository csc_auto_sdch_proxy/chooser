package chooser

import (
	"bytes"
	"encoding/json"
	"net/http"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.com/csc_auto_sdch_proxy/cocaineutils/cerrors"
	"github.com/cocaine/cocaine-framework-go/cocaine"
)

type Context struct {
	MgoSession *mgo.Session
	*cocaine.Logger
	*Config
}

type MultipleSessionErrHandler Context

func (ctx MultipleSessionErrHandler) ServeOrErr(w http.ResponseWriter, r *http.Request) error {
	ctx.Debug("In Multiple Session Hanlder")
	newSession := ctx.MgoSession.Clone()
	defer newSession.Close()
	return SingleSessionErrHandler{
		MgoSession: newSession,
		Logger:     ctx.Logger,
		Config:     ctx.Config,
	}.ServeOrErr(w, r)
}

type SingleSessionErrHandler Context

const (
	mediaTypeJSON     = "application/json"
	contentTypeHeader = "Content-Type"
)

func (ctx SingleSessionErrHandler) ServeOrErr(w http.ResponseWriter, r *http.Request) error {
	ctx.Info("Got request")
	if data, err := json.Marshal(
		struct {
			Method string
			URL    string
			Header http.Header
		}{
			r.Method,
			r.URL.String(),
			r.Header,
		},
	); err == nil {
		ctx.Debugf("Request: %s", data)
	} else {
		ctx.Errf("Request serialization err: %s", err)
	}

	if err := checkRequest(r); err != nil {
		return err
	}
	ctx.Debug("Request checked")

	col := ctx.MgoSession.DB(ctx.Database).C(ctx.Collection)
	ctx.Infof("collection %s", col.FullName)

	iter := col.Find(bson.M{}).Iter()

	var buff bytes.Buffer
	var result bson.M
	buff.WriteString("{\n  \"objects\" : [")
	encoder := json.NewEncoder(&buff)
	{
		first := true
		for iter.Next(&result) {
			if first {
				buff.WriteString("\n    ")
				first = false
			} else {
				buff.WriteString(",\n    ")
			}
			if encodeErr := encoder.Encode(result); encodeErr != nil {
				_ = iter.Close()
				return cerrors.ResponseEncode{encodeErr}
			}
		}
	}
	buff.WriteString("\n  ] \n}")

	if err := iter.Close(); err != nil {
		return cerrors.CustomInternal{"mgo Query error", err}
	}

	ctx.Debug("Sending response")
	w.Header().Set(contentTypeHeader, mediaTypeJSON)
	w.WriteHeader(http.StatusOK)
	if _, repWriteErr := buff.WriteTo(w); repWriteErr != nil {
		ctx.Errf("Error writing query: %s", repWriteErr)
	}
	ctx.Debug("Response done")
	return nil
}

func checkRequest(r *http.Request) error {
	if mediaType := r.Header.Get(contentTypeHeader); mediaType != mediaTypeJSON {
		if mediaType == "" {
			mediaType = "none"
		}
		return cerrors.UnsupportedMediaType{mediaType}
	}
	if r.Method != "GET" {
		return cerrors.UnsupportedMethod{r.Method}
	}
	return nil
}
