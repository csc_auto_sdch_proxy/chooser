package main

import (
	"errors"
	"log"

	"github.com/cocaine/cocaine-framework-go/cocaine"
	mgo "gopkg.in/mgo.v2"

	"bitbucket.com/csc_auto_sdch_proxy/chooser"
	"bitbucket.com/csc_auto_sdch_proxy/cocaineutils/cerrors"
)

func GetContextAndLoop(logger *cocaine.Logger) (err error) {
	var config *chooser.Config
	if config, err = chooser.LoadConfig(); err != nil {
		return cerrors.CustomInternal{"confil load error", err}
	}
	logger.Info("Got config")

	logger.Debug("Try connect to mongo")
	var mgoSession *mgo.Session
	if mgoSession, err = mgo.Dial(config.MongoURI); err != nil {
		return cerrors.CustomInternal{"mongoDB dial error", err}
	}
	defer mgoSession.Close()
	logger.Info("Connected to MongoDB")

	//set up session
	logger.Debug("Setting up mongo")
	mgoSession.SetMode(mgo.Monotonic, true)

	binds := map[string]cocaine.EventHandler{
		"choose": cocaine.WrapHandler(
			cerrors.HTTPHandler{
				chooser.MultipleSessionErrHandler{
					mgoSession,
					logger,
					config,
				},
			},
			logger,
		),
	}
	logger.Debug("Creating Worker")
	var worker *cocaine.Worker
	if worker, err = cocaine.NewWorker(); err != nil {
		return cerrors.CustomInternal{"new worker error", err}
	}
	logger.Info("Worker created")
	worker.Loop(binds)
	return
}

func main() {
	var err error
	var logger *cocaine.Logger
	if logger, err = cocaine.NewLogger(); err != nil {
		log.Fatalf("Can't get logger: %s", err)
	}
	logger.Info("Started")

	err = GetContextAndLoop(logger)
	if err == nil {
		err = errors.New("must not sucsessful return from worker loop")
	}
	logger.Err(err)
	log.Fatal(err)
}
